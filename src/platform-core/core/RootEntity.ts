import ParentEntity from './ParentEntity';
import IRootEntity from '../interface/IRootEntity';
import { staticImplements } from '../utils';

@staticImplements<IRootEntity>()
export default class RootEntity extends ParentEntity {
  private static instance: RootEntity;

  static getInstance() {
    if (!this.instance) {
      this.instance = new RootEntity();
    }

    return this.instance;
  }
}
