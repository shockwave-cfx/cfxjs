import ChildEntity from './ChildEntity';
import ParentEntity from './ParentEntity';
import IGameEntity from '../interface/IGameEntity';
import { NativeEntity } from '../types';

export default class GameEntity extends ChildEntity implements IGameEntity {
  private nativeEntity: NativeEntity;

  constructor(nativeEntity: NativeEntity, parent?: ParentEntity) {
    super(parent);
    this.nativeEntity = nativeEntity;
  }

  getNativeEntity() {
    return this.nativeEntity;
  }
}
