import _ from 'lodash';
import IParentEntity from '../interface/IParentEntity';
import IChildEntity from '../interface/IChildEntity';

export default class ParentEntity implements IParentEntity {
  private children: IChildEntity[] = [];

  getChildren() {
    return this.children;
  }

  addChild(child: IChildEntity) {
    this.children = _.concat(this.children, child);
  }

  removeChild(child: IChildEntity) {
    this.children = _.filter(this.children, child);
  }
}
