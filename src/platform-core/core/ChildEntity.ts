import ParentEntity from './ParentEntity';
import RootEntity from './RootEntity';
import IChildEntity from '../interface/IChildEntity';

export default class ChildEntity extends ParentEntity implements IChildEntity {
  private parent: ParentEntity;

  constructor(parent?: ParentEntity) {
    super();

    this.parent = parent || RootEntity.getInstance();
    this.parent.addChild(this);
  }

  setParent(parent: ParentEntity) {
    this.parent.removeChild(this);
    this.parent = parent;
    this.parent.addChild(this);
  }

  getParent() {
    return this.parent;
  }
}
