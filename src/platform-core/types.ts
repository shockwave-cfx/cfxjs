export type NativeEntity = number;
export type VehicleModel = string;
export type VehicleOptions = {
  [key: string]: any;
};
