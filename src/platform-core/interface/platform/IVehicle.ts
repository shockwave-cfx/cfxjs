import { Vector3 } from 'mathrix';

import IGameEntity from '../IGameEntity';
import IParentEntity from '../IParentEntity';
import { VehicleOptions, VehicleModel, NativeEntity } from '../../types';

export interface IVehicle extends IGameEntity {
  // TODO: Methods, auto-generated?
}

export default interface IVehicleStatic {
  create(
    model: VehicleModel,
    position: Vector3,
    rotation: Vector3,
    options: VehicleOptions,
    parent: IParentEntity,
  ): Promise<IVehicle>;

  fromNative(
    nativeEntity: NativeEntity,
    parent: IParentEntity,
  ): IVehicle;
}
