import IParentEntity from './IParentEntity';

export default interface IChildEntity extends IParentEntity {
  setParent(parent: IParentEntity): void;
  getParent(): IParentEntity;
}
