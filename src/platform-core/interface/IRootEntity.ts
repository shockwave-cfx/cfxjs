import IParentEntity from './IParentEntity';

export interface IRootEntity extends IParentEntity {
  // Empty
}
export default interface IRootEntityStatic {
  getInstance(): IRootEntity;
}
