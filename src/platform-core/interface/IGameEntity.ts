import IChildEntity from './IChildEntity';
import { NativeEntity } from '../types';

export default interface IGameEntity extends IChildEntity {
  getNativeEntity(): NativeEntity;
}
