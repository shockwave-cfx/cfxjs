import IChildEntity from './IChildEntity';

export default interface IParentEntity {
  getChildren(): IChildEntity[];
  addChild(child: IChildEntity): void;
  removeChild(child: IChildEntity): void;
}
