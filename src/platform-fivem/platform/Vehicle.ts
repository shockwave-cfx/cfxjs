import GameEntity from '../../platform-core/core/GameEntity';
import IVehicle from '../../platform-core/interface/platform/IVehicle';
import { staticImplements } from '../../platform-core/utils';

@staticImplements<IVehicle>()
export default class Vehicle extends GameEntity {
  static async create() {
    return new Vehicle(1);
  }

  static fromNative() {
    return new Vehicle(1);
  }
}
