/**
 * Wait until a predicate returns true. Especially useful when you need to wait
 * for an ingame action before continuing, such as IsScreenFadingIn.
 *
 * @example Waits until screen has faded in.
 * DoScreenFadeIn(500);
 * await WaitFor(() => !IsScreenFadingIn());
 *
 * @todo Add a timeout?
 *
 * @param predicate The condition to wait for.
 * @param interval  Interval between checking in ms.
 *
 * @returns A promise that resolves after `predicate` returns true.
 */
export function waitUntil(
  predicate: () => boolean,
  interval = 0,
): Promise<void> {
  return new Promise((resolve) => {
    const timer = setInterval(() => {
      if (predicate()) {
        clearInterval(timer);
        resolve();
      }
    }, interval || 0); // tslint:disable-line
  });
}
