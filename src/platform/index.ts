const PLATFORM_FIVEM = 'PLATFORM_FIVEM';
const PLATFORM_REDM = 'PLATFORM_REDM';

const getPlatform = () => PLATFORM_FIVEM;
class UnsupportedPlatformError extends Error {}

switch (getPlatform()) {
  case PLATFORM_FIVEM:
    module.exports = require('../platform-fivem');
  case PLATFORM_REDM:
    module.exports = require('../platform-redm');
  default:
    throw new UnsupportedPlatformError('Unimplemented platform.');
}
